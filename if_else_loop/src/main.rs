fn main() {
    // if-else
    let score: i32 = 81;
    if score > 49 {
        println!("Passed!");
    } else {
        println!("F!");
    }

    // Loop - for
    let _i: i32 = 0;
    for _i in 0..10{
        println!("{}", _i)
    }

    // Loop - while
    let mut _i: i32 = 0;
    while _i < 10 {
        println!("Hello : {}", _i);
        _i += 1;
    }
}
