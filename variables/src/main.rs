fn main() {
    // Not declare data type to variables
    let number = 99;
    let second_variable = 999;
    let mut a_number = 1;
    
    println!("Second variable is {}", second_variable);
    
    a_number = 2;
    println!("a_number is {a_number}");

    // Declare data type to variables
    let x: i32 = 1; // Integer
    let y: u32 =14; //Unsigned Integer
    let x_f = 3.0; //default float64: f64
    let y_f: f32 = 4.0; //Float 32
}
